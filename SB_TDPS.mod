<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">   
  <UiMod name="SB_TDPS" version="1.1.1" date="11/16/2008">      
    <Author name="Tortall" email="" />          
    <VersionSettings gameVersion="1.9.9" windowsVersion="1.0" savedVariablesVersion="1.0" />        
    <Description text="Tortall's DPS Meter module for snt bar" />      
    <Dependencies>         
      <Dependency name="SNT_BAR" />         
      <Dependency name="Tortall_DPS" />      
    </Dependencies>      
    <Files>         
      <File name="sb_tdps.lua" />      
    </Files>             
    <OnInitialize>         
      <CallFunction name="sb_tdps.Initalize" />      
    </OnInitialize>   
  </UiMod>
</ModuleFile>