sb_tdps = {}

local i_tdps

function sb_tdps.Initalize()
    TortallDPSCore.Register("sb_tdps", sb_tdps.Callback)
    i_tdps = snt_bar.informer:new("i_tdps",nil,135,{255,0,0},nil,nil,"sb_tdps.ToggleMeter",nil)
    
    i_tdps:set_label("DPS")
end

function sb_tdps.ToggleMeter()
    TortallDPSMeter.Toggle()
end

function sb_tdps.Callback(damage, healing, time)
    local dps = damage.Dealt.Total.Amount / time
    if ( time == 0.0 ) then dps = 0 end

    i_tdps:set_label("DPS: "..string.format("%.2f", dps))
end
